A rough and ready extraction of a tombola element used in altLAN events screens.

Intended to be ripped apart for alternative uses, but it does work with an array of items dropped into /dist/index.php.

Briefly...

1) PHP ensures we have at least 8 items, by making duplicates, if necessary.
2) Upon 'spin', JS makes a selection of 8 to populate our 8 sided tombola...
3) ...and a final selection for the 'winner'
4) Subsequent spins will select a new set of 8.

The biggest caveat here then, is that all values provided on page load are given equal chance on all spins, we're not removing values. This functionality came from a different section of the system within the altLAN implimentation. It shouldn't be too difficult to pull something together if needs be.
