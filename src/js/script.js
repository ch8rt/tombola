'use strict';

var spinCounter = 1;

function selector() {

  if ($('.tombola:not(.spinning) .items').length > 0) {

    var container = $('.tombola'),
        selects = [];

    container.find('.items').addClass('pre').each(function () {

      var attendees = $(this).find('.item').removeClass('selected'),
          selected = $(shuffle(attendees)).slice(0, 1),
          spinMaster = 1755, // 1755 being the magic number for the last side of the tombola (with 8 sides)
          spin = spinMaster + 720 * spinCounter,
          duration = 1800,
          stepcount = 1,
          cloneNo = 0;

      if (spinCounter > 1) {
        $(this).removeClass('pre');
      }
      $(this).removeClass('animation_ending');
      container.addClass('spinning');

      // Animate
      $(this).animate({ borderSpacing: spin * .9 }, {
        step: function step(now, fx) {
          $(this).css('transform', 'translate(-50%,-50%) rotateX(' + now + 'deg) translateZ(-480px)');
          if (stepcount < 2) {
            $(this).find('.selected').removeClass('selected');
            selected.addClass('selected').appendTo($(this));
            selects.push(selected.attr('data-attendee'));
          }
          stepcount++;
        },
        duration: duration * .25,
        easing: 'linear'
      }).animate({ borderSpacing: spin }, {
        step: function step(now, fx) {
          $(this).css('transform', 'translate(-50%,-50%) rotateX(' + now + 'deg) translateZ(-480px)');
          $(this).addClass('animation_ending');
        },
        duration: duration * .75,
        easing: 'easeOutElastic',
        complete: function() {
          container.removeClass('spinning');
        }
      });
    });

    spinCounter++;
  }
}

function shuffle(array) {
  var m = array.length,
      t,
      i;
  while (m) {
    i = Math.floor(Math.random() * m--);
    t = array[m];
    array[m] = array[i];
    array[i] = t;
  }
  return array;
}

jQuery.extend(jQuery.easing, {
  def: 'easeOutQuad',
  easeOutElastic: function easeOutElastic(x, t, b, c, d) {
    var s = 1.70158;var p = 0;var a = c;
    if (t == 0) return b;if ((t /= d) == 1) return b + c;if (!p) p = d * .3;
    if (a < Math.abs(c)) {
      a = c;var s = p / 4;
    } else var s = p / (2 * Math.PI) * Math.asin(c / a);
    return a * Math.pow(2, -10 * t) * Math.sin((t * d - s) * (2 * Math.PI) / p) + c + b;
  },
});

$('.tombola_btn').click( function() {
  selector();
});

selector();
