<?php

include_once('components/tombola.php');

$items = array(
  'One',
  'Two',
  'Three',
  'Four',
  'Five',
  'Six',
  'Seven',
  'Eight',
  'Nine',
  'Ten',
  'Eleven',
  'Twelve',
  'Thirteen',
  'Fourteen',
  'Fifteen'
);

?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Tombola</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="/assets/style.css?v=0.0.2">
</head>
<body>
  <div class="tombola">
    <?php echo tombola_init($items); ?>
    <div class="tombola_btn">Spin</div>
  </div>
	<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
	<script src="/assets/script.js?v=0.0.3"></script>
</body>
</html>
