<?php

function tombola_init($items) {

  // ensure we have at least 8 items
  for ($i=0; $i < 4; $i++) {
    if( count($items) < 8 ) {
      $items = array_merge($items,$items);
    }
  }

  // shuffle
  shuffle($items);

  $html_items = '';
  foreach( $items as $x ) {
    $html_items .= '<div class="item"><div class="item_name">' . $x . '</div></div>';
  }

  return '<div class="items animation_ending" style="transform: translate(-50%,-50%) rotateX(2475deg) translateZ(-480px); border-spacing: 2475px;">' . $html_items . '</div>';

}

?>
