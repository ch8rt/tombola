var gulp = require('gulp'),
	browserSync = require('browser-sync').create(),
	concat = require('gulp-concat'),
	uglify = require('gulp-uglify'),
	sass = require('gulp-sass'),
	autoprefixer = require('gulp-autoprefixer'),
	plumber = require('gulp-plumber');

gulp.task('watch', ['browserSync', 'script', 'styles'], function() {
	gulp.watch('src/js/**/*.js', ['script']);
	gulp.watch('src/css/**/*.scss', ['styles']);
	gulp.watch('dist/**/*.+(html|php)', browserSync.reload);
});

gulp.task('script', () => {
	return gulp.src('src/js/**/*.js')
		.pipe(plumber())
		.pipe(concat('script.js'))
		.pipe(uglify())
		.pipe(gulp.dest('dist/assets'))
		.pipe(browserSync.reload({
			stream: true
		}))
});

gulp.task('styles', function() {
	return gulp.src('src/css/[^_]*.scss')
		.pipe(plumber())
		.pipe(sass({
			outputStyle: 'compressed'
		}))
		.pipe(autoprefixer({
			browsers: ['last 4 versions'],
			cascade: false
		}))
		.pipe(gulp.dest('dist/assets'))
		.pipe(browserSync.reload({
			stream: true
		}))
});

gulp.task('browserSync', function() {
	browserSync.init({
		proxy: 'tombola.test/'
	})
});
